This group serves as a FOSS Repository for the Aristotle University of Thessaloniki.

Feel free to join by requesting access, and start uploading your own projects.